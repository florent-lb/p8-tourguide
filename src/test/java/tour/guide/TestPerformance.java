package tour.guide;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.Before;
import org.junit.Test;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tour.guide.configuration.ExecutorServiceConfiguration;
import tour.guide.model.user.User;
import tour.guide.service.GpsUtilsFix;
import tour.guide.service.RewardsService;
import tour.guide.service.TourGuideService;

@Slf4j
public class TestPerformance {

	GpsUtilsFix gpsUtil;
	RewardsService rewardsService;
	ExecutorService executor;
	TourGuideService tourGuideService;

	@Before
	public void setup()
	{
		ExecutorServiceConfiguration config = new ExecutorServiceConfiguration();
		executor = config.taskExecutor();

		gpsUtil = new GpsUtilsFix();
		rewardsService = new RewardsService(gpsUtil, new RewardCentral(),executor);



		tourGuideService = new TourGuideService(gpsUtil, rewardsService,executor);
	}

	/*
	 * A note on performance improvements:
	 *     
	 *     The number of users generated for the high volume tests can be easily adjusted via this method:
	 *     
	 *     		InternalTestHelper.setInternalUserNumber(100000);
	 *     
	 *     
	 *     These tests can be modified to suit new solutions, just as long as the performance metrics
	 *     at the end of the tests remains consistent. 
	 * 
	 *     These are performance metrics that we are trying to hit:
	 *     
	 *     highVolumeTrackLocation: 100,000 users within 15 minutes:
	 *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
	 *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	 */

	@Test
	public void highVolumeTrackLocation() {

		// Set up the number of user in application.properties
		List<User> allUsers = tourGuideService.getAllUsers();
		List<Future<VisitedLocation>> allLocations = new ArrayList<>();
		
	    StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		for(User user : allUsers) {
			allLocations.add(tourGuideService.trackUserLocation(user));
		}

		boolean stayIn;
		do
		{
			long tasksdone = allLocations.stream().filter(Future::isDone).count();
			stayIn = tasksdone != allUsers.size();

		}while (stayIn);

		stopWatch.stop();

		System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}

	@Test
	public void highVolumeGetRewards() throws InterruptedException {

		// Set up the number of user in application.properties
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		
	    Attraction attraction = gpsUtil.getAttractions().get(0);
		List<User> allUsers = tourGuideService.getAllUsers();
		allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));
	     
	    allUsers.forEach(rewardsService::calculateRewards);
		executor.shutdown();
		executor.awaitTermination(20,TimeUnit.MINUTES);
	    
		for(User user : allUsers) {
			assertTrue(user.getUserRewards().size() > 0);
		}
		stopWatch.stop();

		System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds."); 
		assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
	}
	
}
