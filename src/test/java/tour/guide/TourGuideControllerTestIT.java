package tour.guide;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import rewardCentral.RewardCentral;
import tour.guide.configuration.ExecutorServiceConfiguration;
import tour.guide.model.ProposalAttraction;
import tour.guide.service.GpsUtilsFix;
import tour.guide.service.RewardsService;
import tour.guide.service.TourGuideService;
import tour.guide.view.TourGuideController;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import java.util.concurrent.ExecutorService;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
@ContextConfiguration(classes = {
        TourGuideController.class, TourGuideService.class, GpsUtilsFix.class, RewardsService.class, RewardCentral.class,
        ExecutorService.class, ExecutorServiceConfiguration.class
})
@Slf4j
public class TourGuideControllerTestIT {

    @LocalServerPort
    private int localPort;

    private String baseUri;

    private RestTemplate template;

    private Jsonb jsonBinder;


    @BeforeEach
    public void setup() {
        template = new RestTemplate();
        baseUri = "http://localhost:" + localPort;
        jsonBinder = JsonbBuilder.create();
    }

    @SneakyThrows
    @AfterEach
    public void cleanUp()
    {
        jsonBinder.close();
    }
    @Test
    @DisplayName("GET location")
    public void get_whenAskForUserLocation_shouldReturnLastVisitedLocation() {


        String uri = UriComponentsBuilder.fromUriString(baseUri)
                .path("/getLocation")
                .queryParam("userName", "internalUser1").toUriString();
        logger.info("URI tested : " + uri);
        ResponseEntity<String> location = template
                .getForEntity(uri, String.class);

        assertThat(location)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode).extracting(HttpStatus::is2xxSuccessful).isEqualTo(Boolean.TRUE);

        assertThat(location)
                .extracting(HttpEntity::hasBody)
                .isEqualTo(Boolean.TRUE);

    }

    @Test
    @DisplayName("GET nearby attractions")
    public void get_whenAskNearbyAttraction_shouldReturnLotOfLocations() {
        String uri = UriComponentsBuilder.fromUriString(baseUri)
                .path("getNearbyAttractions")
                .queryParam("userName", "internalUser1").toUriString();
        logger.info("URI tested : " + uri);
        ResponseEntity<String> location = template
                .getForEntity(uri, String.class);

        assertThat(location)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode).extracting(HttpStatus::is2xxSuccessful).isEqualTo(Boolean.TRUE);

        assertThat(location)
                .extracting(HttpEntity::hasBody)
                .isEqualTo(Boolean.TRUE);

        assertThat( jsonBinder.fromJson(location.getBody(), ProposalAttraction.class).getAttractionToUsers()).hasSize(5);

    }

    @Test
    @DisplayName("GET reward")
    public void get_whenAskForUserRewards_shouldReturnOkWithBody() {


        String uri = UriComponentsBuilder.fromUriString(baseUri)
                .path("/getRewards")
                .queryParam("userName", "internalUser1").toUriString();
        logger.info("URI tested : " + uri);
        ResponseEntity<String> location = template
                .getForEntity(uri, String.class);

        assertThat(location)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode).extracting(HttpStatus::is2xxSuccessful).isEqualTo(Boolean.TRUE);

        assertThat(location)
                .extracting(HttpEntity::hasBody)
                .isEqualTo(Boolean.TRUE);

    }

    @Test
    @DisplayName("GET all current Location")
    public void get_whenAskForCurrentUserLocation_shouldReturnOkWithBody() {

        String uri = UriComponentsBuilder.fromUriString(baseUri)
                .path("/getAllCurrentLocations").toUriString();
        logger.info("URI tested : " + uri);
        ResponseEntity<String> location = template
                .getForEntity(uri, String.class);

        assertThat(location)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode).extracting(HttpStatus::is2xxSuccessful).isEqualTo(Boolean.TRUE);

        assertThat(location)
                .extracting(HttpEntity::hasBody)
                .isEqualTo(Boolean.TRUE);

    }
    @Test
    @DisplayName("GET Trip Deals")
    public void get_whenAskForTripDeals_shouldReturnOkWithBody() {

        String uri = UriComponentsBuilder.fromUriString(baseUri)
                .path("/getTripDeals")
                .queryParam("userName", "internalUser1").toUriString();

        logger.info("URI tested : " + uri);
        ResponseEntity<String> location = template
                .getForEntity(uri, String.class);

        assertThat(location)
                .isNotNull()
                .extracting(ResponseEntity::getStatusCode).extracting(HttpStatus::is2xxSuccessful).isEqualTo(Boolean.TRUE);

        assertThat(location)
                .extracting(HttpEntity::hasBody)
                .isEqualTo(Boolean.TRUE);

    }
}
