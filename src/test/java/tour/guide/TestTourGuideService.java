package tour.guide;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tour.guide.configuration.ExecutorServiceConfiguration;
import tour.guide.model.ProposalAttraction;
import tour.guide.service.GpsUtilsFix;
import tour.guide.service.RewardsService;
import tour.guide.service.TourGuideService;
import tour.guide.model.user.User;
import tripPricer.Provider;

import static org.junit.Assert.*;

public class TestTourGuideService {


	GpsUtilsFix gpsUtil;
	RewardsService rewardsService;
	ExecutorService executor;
	TourGuideService tourGuideService;

	@Before
	public void setup()
	{
		ExecutorServiceConfiguration config = new ExecutorServiceConfiguration();
		executor = config.taskExecutor();

		gpsUtil = new GpsUtilsFix();
		rewardsService = new RewardsService(gpsUtil, new RewardCentral(),executor);



		tourGuideService = new TourGuideService(gpsUtil, rewardsService,executor);
	}

	@Test
	public void getUserLocation() throws ExecutionException, InterruptedException {
		// Set up the number of user in application.properties
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();
		assertEquals(visitedLocation.userId, user.getUserId());
	}
	
	@Test
	public void addUser() {
		// Set up the number of user in application.properties
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		User retrivedUser = tourGuideService.getUser(user.getUserName());
		User retrivedUser2 = tourGuideService.getUser(user2.getUserName());

		
		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}
	
	@Test
	public void getAllUsers() {
		// Set up the number of user in application.properties
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		tourGuideService.addUser(user);
		tourGuideService.addUser(user2);
		
		List<User> allUsers = tourGuideService.getAllUsers();

		
		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}
	
	@Test
	public void trackUser() throws ExecutionException, InterruptedException {
		// Set up the number of user in application.properties
		
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackUserLocation(user).get();

		
		assertEquals(user.getUserId(), visitedLocation.userId);
	}

	@Test
	public void getNearbyAttractions()  {
		// Set up the number of user in application.properties
		ProposalAttraction bestPropositionForUser = tourGuideService.getBestPropositionForUser("internalUser1");

		
		assertEquals(5, bestPropositionForUser.getAttractionToUsers().size());
	}

	@Test
	public void getTripDeals() {
		// Set up the number of user in application.properties
		List<Provider> providers = tourGuideService.getTripDeals("internalUser1");

		Assertions.assertThat((providers.size())).isGreaterThan(0);
	}
	
	
}
