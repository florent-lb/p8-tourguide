package tour.guide;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tour.guide.configuration.ExecutorServiceConfiguration;
import tour.guide.service.GpsUtilsFix;
import tour.guide.service.RewardsService;
import tour.guide.service.TourGuideService;
import tour.guide.model.user.User;
import tour.guide.model.user.UserReward;

public class TestRewardsService {


	GpsUtilsFix gpsUtil;
	RewardsService rewardsService;
	ExecutorService executor;
	TourGuideService tourGuideService;

	@Before
	public void setup()
	{
		ExecutorServiceConfiguration config = new ExecutorServiceConfiguration();
		executor = config.taskExecutor();
		gpsUtil = new GpsUtilsFix();
		rewardsService = new RewardsService(gpsUtil, new RewardCentral(),executor);



		tourGuideService = new TourGuideService(gpsUtil, rewardsService,executor);
	}

	@Test
	public void userGetRewards() throws InterruptedException {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		Attraction attraction = gpsUtil.getAttractions().get(0);
		user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
		rewardsService.calculateRewards(user);
		executor.shutdown();
		executor.awaitTermination(2,TimeUnit.MINUTES);

		Assertions.assertThat(user.getUserRewards().size()).isEqualTo(1);
	}
	
	@Test
	public void isWithinAttractionProximity() {
		Attraction attraction = gpsUtil.getAttractions().get(0);
		assertTrue(rewardsService.isWithinAttractionProximity(attraction, attraction));
	}

	@Test
	public void nearAllAttractions() throws InterruptedException {
		rewardsService.setProximityBuffer(Integer.MAX_VALUE);

		// Set up the number of user in application.properties

		User user = tourGuideService.getAllUsers().get(0);
		rewardsService.calculateRewards(user);
		executor.shutdown();
		executor.awaitTermination(2, TimeUnit.MINUTES);
		List<UserReward> userRewards = tourGuideService.getUserRewards(user.getUserName());

		assertEquals(gpsUtil.getAttractions().size(), userRewards.size());
	}
	
}
