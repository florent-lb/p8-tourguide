package tour.guide.service;

import com.google.common.util.concurrent.RateLimiter;
import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.extern.slf4j.Slf4j;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Slf4j
public class GpsUtilsFix  extends GpsUtil {

    private static final RateLimiter rateLimiter = RateLimiter.create(1000.0D);

    @Override
    public VisitedLocation getUserLocation(UUID userId) {

        DecimalFormat format = new DecimalFormat("###,######");
        rateLimiter.acquire();
        waitRandom();
        double longitude = ThreadLocalRandom.current().nextDouble(-180.0D, 180.0D);
        longitude = Double.parseDouble(format.format(longitude));
        double latitude = ThreadLocalRandom.current().nextDouble(-85.05112878D, 85.05112878D);
        latitude = Double.parseDouble(format.format(latitude));
        return new VisitedLocation(userId, new Location(latitude, longitude), new Date());
    }


    private void waitRandom() {
        int random = ThreadLocalRandom.current().nextInt(30, 100);

        try {
            TimeUnit.MILLISECONDS.sleep((long)random);
        } catch (InterruptedException e) {
            logger.error("Impossible to sleep this thread",e);
            Thread.currentThread().interrupt();
        }

    }
}
