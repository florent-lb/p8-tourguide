package tour.guide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tour.guide.model.user.User;
import tour.guide.model.user.UserReward;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

@Service
@Slf4j
public class RewardsService {
    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

    // proximity in miles
    private int defaultProximityBuffer = 10;
    @Setter
    private int proximityBuffer = defaultProximityBuffer;
    private int attractionProximityRange = 200;
    private final GpsUtilsFix gpsUtil;
    private final RewardCentral rewardsCentral;
    private final ExecutorService executorService;

    public RewardsService(GpsUtilsFix gpsUtil, RewardCentral rewardCentral, @Qualifier("task-guide-executor") ExecutorService executorService) {
        this.gpsUtil = gpsUtil;
        this.rewardsCentral = rewardCentral;
        this.executorService = executorService;
    }

    public void calculateRewards(User user) {
        executorService.submit(() ->
        {
            CopyOnWriteArrayList<VisitedLocation> userLocations = new CopyOnWriteArrayList<>(user.getVisitedLocations());
            List<Attraction> attractions = gpsUtil.getAttractions();
            attractions
                    .stream()
                    .filter(attraction -> !attractionVisited(user.getUserRewards(), attraction))
                    .forEach(attraction ->
                            userLocations
                                    .stream()
                                    .filter(visitedLocation -> nearAttraction(visitedLocation, attraction))
                                    .findFirst()
                                    .ifPresent(visitedLocation ->
                                            user.getUserRewards().add(new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user))))
                    );
        });


    }

    private boolean attractionVisited(List<UserReward> rewards, Attraction attraction) {
        return rewards
                .stream()
                .map(UserReward::getAttraction)
                .map(attractionReward -> attractionReward.attractionName)
                .anyMatch(attraction.attractionName::equalsIgnoreCase);
    }


    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        return getDistance(attraction, location) < attractionProximityRange;
    }

    private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        return getDistance(attraction, visitedLocation.location) < proximityBuffer;
    }

    private int getRewardPoints(Attraction attraction, User user) {
        return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
    }

    public Integer getRewardPoints(UUID attractionId, UUID userId) {
        return rewardsCentral.getAttractionRewardPoints(attractionId, userId);
    }

    public double getDistance(Location loc1, Location loc2) {
        double lat1 = Math.toRadians(loc1.latitude);
        double lon1 = Math.toRadians(loc1.longitude);
        double lat2 = Math.toRadians(loc2.latitude);
        double lon2 = Math.toRadians(loc2.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }

}
