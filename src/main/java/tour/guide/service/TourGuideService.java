package tour.guide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import tour.guide.model.ProposalAttraction;
import tour.guide.model.RewardAttractionToUser;
import tour.guide.model.user.User;
import tour.guide.model.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class TourGuideService {
    private Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    private final GpsUtilsFix gpsUtil;
    private final RewardsService rewardsService;
    private final TripPricer tripPricer = new TripPricer();
    private final ExecutorService executor;
    private Properties properties;

    boolean testMode = true;

    public TourGuideService(GpsUtilsFix gpsUtil, RewardsService rewardsService, @Qualifier("task-guide-executor") ExecutorService executor) {
        this.gpsUtil = gpsUtil;
        this.rewardsService = rewardsService;
        this.executor = executor;
        try {
            properties = new Properties();
            properties.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
            if (testMode) {
                logger.info("TestMode enabled");
                logger.debug("Initializing users");
                initializeInternalUsers();
                logger.debug("Finished initializing users");
            }
        } catch (IOException e) {
            throw new NullPointerException("Impossible to load the number users to generate, please verify your application.properties set the value \"test.user.size\"");
        }
    }

    public List<UserReward> getUserRewards(String userName) {
        return getUser(userName).getUserRewards();
    }

    public Future<VisitedLocation> getUserLocation(User user) {


        return Optional.of(user)
                .map(myUser -> executor.submit(myUser::getLastVisitedLocation))
                .orElseGet(() -> trackUserLocation(user));
    }

    /**
     * Used for return a location in the current Thread and note in concurrency system
     *
     * @param userName the name of the user
     * @return le last location of the user
     */
    public VisitedLocation getUserLocationNoFuture(String userName) {
        User user = getUser(userName);
        Future<VisitedLocation> location = getUserLocation(user);
        try {
            return location.get(8, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.warn("Impossible to retreive the location of the user : {}",user);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    public User getUser(String userName) {
        return Optional.ofNullable(internalUserMap.get(userName)).orElseThrow(() -> new NullPointerException("No user found for : " + userName));
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(internalUserMap.values());
    }

    public void addUser(User user) {
        if (!internalUserMap.containsKey(user.getUserName())) {
            internalUserMap.put(user.getUserName(), user);
        }
    }

    public List<Provider> getTripDeals(String userName) {
        User user = getUser(userName);
        int cumulatativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();
        List<Provider> providers = tripPricer.getPrice(TRIP_PRICER_API_KEY,
                user.getUserId(),
                user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(),
                user.getUserPreferences().getTripDuration(),
                cumulatativeRewardPoints);
        user.setTripDeals(providers);
        return providers;
    }

    public Future<VisitedLocation> trackUserLocation(User user) {

        return executor.submit(() ->
        {
            VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());
            user.addToVisitedLocations(visitedLocation);
            rewardsService.calculateRewards(user);
            return visitedLocation;

        });
    }

    /**
     * @param userName the name of the user
     * @return a {@link ProposalAttraction}
     * @throws NullPointerException if the user doesn't exist
     */
    public ProposalAttraction getBestPropositionForUser(String userName) {

        //Get User and his location
        User user = getUser(userName);

        try {
            Location location = getUserLocation(user).get().location;
            //Calculate 5 first locations
            List<RewardAttractionToUser> rewards = getNearByAttractions(location);
            rewards
                    .forEach(rewardAttractionToUser ->
                            rewardAttractionToUser
                                    .setRewardPoints(rewardsService.getRewardPoints(rewardAttractionToUser.getAttractionId(), user.getUserId()))
                    );
            return ProposalAttraction.builder()
                    .attractionToUsers(rewards)
                    .userLatitude(location.latitude)
                    .userLongitude(location.longitude)
                    .build();

        } catch (ExecutionException e) {
            logger.error("Execution error on the thread ", e);
            throw new NullPointerException("Impossible to retrieve the best Location");
        } catch (InterruptedException e) {
            logger.error("Thread interrupted ", e);
            Thread.currentThread().interrupt();
            throw new NullPointerException("Impossible to retrieve the best Location");
        }


    }

    /**
     * @param visitedLocation The current location to inspect
     * @return all {@link RewardAttractionToUser} each object contains the attractions near by the user with its reward
     */
    public List<RewardAttractionToUser> getNearByAttractions(Location visitedLocation) {

        return gpsUtil.getAttractions()
                .parallelStream()
                .map(attraction ->
                        calcRewardAttractionToUser(attraction, visitedLocation))
                .sorted()
                .limit(Long.parseLong(properties.getProperty("attractions.proximity.max")))
                .collect(Collectors.toList());

    }

    /**
     * Build an object {@link RewardAttractionToUser}
     *
     * @param attraction The attraction to add
     * @param location   the location to add
     * @return the new  {@link RewardAttractionToUser}
     */
    public RewardAttractionToUser calcRewardAttractionToUser(Attraction attraction, Location location) {
        return RewardAttractionToUser
                .builder()
                .attractionId(attraction.attractionId)
                .attractionLatitude(attraction.latitude)
                .attractionLongitude(attraction.longitude)
                .attractionName(attraction.attractionName)
                .distance(rewardsService.getDistance(attraction, location))
                .build();
    }

    /**
     * @return All last location of users with userId in key and location in value
     */
    public Map<UUID, Location> getMappingLocationUser() {
        return internalUserMap.values()
                .stream()
                .filter(user -> user.getLastVisitedLocation() != null)
                .collect(Collectors.toMap(User::getUserId, user -> user.getLastVisitedLocation().location));

    }


    /**********************************************************************************
     *
     * Methods Below: For Internal Testing
     *
     **********************************************************************************/
    private static final String TRIP_PRICER_API_KEY = "test-server-api-key";
    // Database connection will be used for external users, but for testing purposes internal users are provided and stored in memory
    private final Map<String, User> internalUserMap = new HashMap<>();

    private void initializeInternalUsers() {


        Integer nbUser = Integer.parseInt(properties.getProperty("test.user.size"));

        IntStream.range(0, nbUser).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            internalUserMap.put(userName, user);
        });
        logger.debug("Created {} internal test users.", nbUser);
    }

    private void generateUserLocationHistory(User user) {
        IntStream.range(0, 3).forEach(i ->
                user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(generateRandomLatitude(), generateRandomLongitude()), getRandomTime()))
        );
    }

    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }


}
