package tour.guide.view;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tour.guide.model.ProposalAttraction;
import tour.guide.model.user.UserReward;
import tour.guide.service.TourGuideService;
import tripPricer.Provider;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class TourGuideController {

    final TourGuideService tourGuideService;

    public TourGuideController(TourGuideService tourGuideService) {
        this.tourGuideService = tourGuideService;
    }

    /**
     * Simple home context request
     *
     * @return Greetings
     */
    @GetMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    /**
     * Return the location of the user
     *
     * @param userName name of the user
     * @return {@link VisitedLocation}, location with longitude and latitude and the date of the travel
     */
    @GetMapping(value = "/getLocation", produces = APPLICATION_JSON_VALUE)
    public VisitedLocation getLocation(@RequestParam String userName) {
        return tourGuideService.getUserLocationNoFuture(userName);
    }

    /**
     * Get the number of attraction define in application.properties around the user pass thought parameter
     *
     * @param userName the name of the user
     * @return List of nearby attractions encapsulate in object {@link ProposalAttraction} with the longitude and latitude of the user asked
     */
    @GetMapping(value = "/getNearbyAttractions", produces = APPLICATION_JSON_VALUE)
    public ProposalAttraction getNearbyAttractions(@RequestParam String userName) {
        return tourGuideService.getBestPropositionForUser(userName);
    }

    /**
     * Return all reward of the user
     *
     * @param userName the name of the user
     * @return List of {@link UserReward}
     */
    @GetMapping(value = "/getRewards", produces = APPLICATION_JSON_VALUE)
    public List<UserReward> getRewards(@RequestParam String userName) {
        return tourGuideService.getUserRewards(userName);
    }

    /**
     * @return All current users's location known in a map with UUID of the user in key and his {@link Location}
     */
    @GetMapping(value = "/getAllCurrentLocations", produces = APPLICATION_JSON_VALUE)
    public Map<UUID, Location> getAllCurrentLocations() {
        return tourGuideService.getMappingLocationUser();
    }

    /**
     * @param userName the name of the user
     * @return List of the deal's provider with the offer
     */
    @GetMapping(value = "/getTripDeals", produces = APPLICATION_JSON_VALUE)
    public List<Provider> getTripDeals(@RequestParam String userName) {
        return tourGuideService.getTripDeals(userName);

    }


}