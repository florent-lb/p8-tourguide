package tour.guide.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@RequiredArgsConstructor
@Builder
@AllArgsConstructor
@EqualsAndHashCode(of = {"attractionId"})
@NoArgsConstructor
public class RewardAttractionToUser implements Comparable<RewardAttractionToUser> {

    @NonNull
    private Double attractionLongitude;
    @NonNull
    private Double attractionLatitude;
    @NonNull
    private String attractionName;

    @JsonIgnore
    private UUID attractionId;

    private Double distance;

    private int rewardPoints;

    @Override
    public int compareTo(@NotNull RewardAttractionToUser user) {
        return Double.compare(this.distance, user.distance);
    }
}
