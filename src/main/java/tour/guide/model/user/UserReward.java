package tour.guide.model.user;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import lombok.*;

@EqualsAndHashCode(of = {"visitedLocation","attraction"})
@Getter @Setter
@RequiredArgsConstructor
@AllArgsConstructor
public class UserReward {

	public final VisitedLocation visitedLocation;
	public final Attraction attraction;
	private int rewardPoints;

	@Override
	public String toString() {
		return "UserReward{" +
				"visitedLocation=" + visitedLocation.location.longitude +":" +visitedLocation.location.longitude +
				", attraction=" + attraction.attractionName +
				", rewardPoints=" + rewardPoints +
				'}';
	}
}
