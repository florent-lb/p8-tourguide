package tour.guide.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rewardCentral.RewardCentral;
import tour.guide.service.GpsUtilsFix;
import tour.guide.service.RewardsService;

import java.util.concurrent.ExecutorService;

@Configuration
public class TourGuideModule {

	@Bean
	public GpsUtilsFix getGpsUtil() {
		return new GpsUtilsFix();
	}
	
	@Bean
	public RewardsService getRewardsService(@Qualifier("task-guide-executor") ExecutorService executorService) {
		return new RewardsService(getGpsUtil(), getRewardCentral(),executorService);
	}
	
	@Bean
	public RewardCentral getRewardCentral() {
		return new RewardCentral();
	}
	
}
