package tour.guide.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@Slf4j
public class ExecutorServiceConfiguration {

    @Value("${tour.guide.concurrency.executor.max.pool.size}")
    private String maxCoreSize;

    @Bean("task-guide-executor")
    public ExecutorService taskExecutor()
    {
        maxCoreSize = Optional.ofNullable(maxCoreSize).orElse("1000");
        logger.info("Initialize ExecutorService with Fixed ThreadPool("+maxCoreSize+")");
        return Executors.newCachedThreadPool();
    }
}
