[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_p8-tourguide&metric=bugs)](https://sonarcloud.io/dashboard?id=florent-lb_p8-tourguide)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_p8-tourguide&metric=code_smells)](https://sonarcloud.io/dashboard?id=florent-lb_p8-tourguide)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_p8-tourguide&metric=coverage)](https://sonarcloud.io/dashboard?id=florent-lb_p8-tourguide)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=florent-lb_p8-tourguide&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=florent-lb_p8-tourguide)
# TourGuide
A simple app for check the user location, ask for reward and found nearby attractions.


